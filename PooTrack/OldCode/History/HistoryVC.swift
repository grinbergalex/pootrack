//
//  HistoryVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 17/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class HistoryVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var historyItems = [HistoryModel]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backTopBarButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        
        fetchHistory()

    }
    
    func fetchHistory() {
        
        let uid = Auth.auth().currentUser?.uid
        
        let DB_BASE = Database.database().reference().child("USERS").child(uid!).child("Walks")
        DB_BASE.observe(.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = HistoryModel()
                index.imageUrl = (dictionary["profileImageUrl"] as! String?)!
                index.dogUid = (dictionary["doguid"] as! String?)!
                index.distance = (dictionary["distance"] as! String?)!
                index.duration = (dictionary["duration"] as! String?)!
                index.weekDay = (dictionary["weekDay"] as! String?)!
                self.historyItems.append(index)
                
                let url = URLRequest(url: URL(string: index.imageUrl)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                }
                
                task.resume()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    self.tableView.reloadData()
                }
            }
            print(snapshot)
        }, withCancel: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        let info = historyItems[indexPath.row]
        
        cell.historyDate.text = info.weekDay
        cell.historyDogName.text = info.name
        cell.historyDuration.text = info.duration
        cell.historyDistance.text = info.distance

        cell.historyDogImage.image = nil
        
        cell.tag += 1
        let tag = cell.tag
        
        let photoUrl =  historyItems[indexPath.row].imageUrl  //posts[indexPath.row].photoUrl
        
        self.getImage(url: photoUrl) { photo in
            if photo != nil {
                if cell.tag == tag {
                    DispatchQueue.main.async {
                        cell.historyDogImage.image = photo
                    }
                }
            }
        }
        
        
        return cell
    }
    
    @IBAction func goBackAction( _sender: UIButton) {
        self.performSegue(withIdentifier: "DogMainCode", sender: self)
    }

}

extension UIViewController {
    
    func getImage(url: String, completion: @escaping (UIImage?) -> ()) {
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            if error == nil {
                completion(UIImage(data: data!))
            } else {
                completion(nil)
            }
            }.resume()
    }
    
}
