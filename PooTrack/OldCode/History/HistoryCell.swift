//
//  HistoryCell.swift
//  PooTrack
//
//  Created by Alex Grinberg on 17/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var historyDate: UILabel!
    @IBOutlet weak var historyDogName: UILabel!
    @IBOutlet weak var historyDuration: UILabel!
    @IBOutlet weak var historyDistance: UILabel!
    @IBOutlet weak var historyDogImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
