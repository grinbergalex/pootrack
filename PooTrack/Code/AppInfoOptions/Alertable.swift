//
//  Alertable.swift
//  PooTrack
//
//  Created by Alex Grinberg on 24/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {
    
func newAlert(title: String, message: String) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    
    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
    
}
func newAlertCancel(title: String, message: String) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    
    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
    
    }
    
    func AlertWithAnswer(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }

}
