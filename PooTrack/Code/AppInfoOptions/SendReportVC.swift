//
//  SendReportVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 24/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class SendReportVC: UIViewController {
    
    let textInfo: UITextView = {
       let text = UITextView()
        text.text = "Please report you issue.\nTry to be as detailed as possible."
        text.font = UIFont(name: "GillSans", size: 17)
        text.textAlignment = .center
        
        return text
        
    }()
    
    let logoImage: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "Logo Design Dog Schedule -2 Transparent")
        logo.contentMode = .scaleAspectFit
        
        return logo
        
    }()
    
    let sendReportButton: UIButton = {
        let button = UIButton()
        button.setTitle("Send Report", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans-Bold", size: 15)
        button.backgroundColor = UIColor.rgb(red: 82, green: 162, blue: 131)
        button.addTarget(self, action: #selector(sendReport(_:)), for: .touchUpInside)
        return button
        
    }()
    
    let backTopBarButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "backButton"), for: .normal)
        button.addTarget(self, action: #selector(goBackAction(_sender:)), for: .touchUpInside)
        return button
        
    }()
    
    let topView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 82, green: 162, blue: 131)
        return view
        
    }()

    
    let uid = Auth.auth().currentUser?.uid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.rgb(red: 252, green: 251, blue: 250)
        setupView()
        
        for family in UIFont.familyNames {
            
            let sName: String = family as String
            print("family: \(sName)")
            
            for name in UIFont.fontNames(forFamilyName: sName) {
                print("name: \(name as String)")
            }
        }

    }
    
    func setupView() {
        //Top View Contraints
        view.addSubview(topView)
        topView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        
        //Bar Back button contraints
        view.addSubview(backTopBarButton)
        backTopBarButton.anchor(top: topView.topAnchor, left: topView.leftAnchor, bottom: topView.bottomAnchor, right: nil, paddingTop: 45, paddingLeft: 20, paddingBottom: 20, paddingRight: 0, width: 30, height: 30)
        
        //Logo from App
        view.addSubview(logoImage)
        logoImage.anchor(top: topView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 15, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 150)
        
        //TextView
        view.addSubview(textInfo)
        textInfo.anchor(top: logoImage.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 15, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 400)
        
        //Bar Back button contraints
        view.addSubview(sendReportButton)
        sendReportButton.anchor(top: textInfo.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 15, paddingLeft: 30, paddingBottom: 50, paddingRight: 30, width: 0, height: 40)
    }
    
    @objc func sendReport(_ sender: UIButton) {
        
        guard let feedbacktext = textInfo.text else {return}
        
        self.newAlertCancel(title: "Do you wanna send the report?", message: "Make sure all the information is correct")
        
        if feedbacktext.isEmpty == true {
            sendReportButton.isEnabled = false
            sendReportButton.layer.opacity = 0.5
        } else {
            sendReportButton.isEnabled = true
            sendReportButton.layer.opacity = 1.0
            
            let values = ["userUid": uid, "feedbackText": textInfo.text]
            let ref = Database.database().reference().child("FEEDBACK")
            ref.child(uid!).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(error, ref) in
                
                if error != nil {
                    print("Feedback Saved")
                    self.newAlert(title: "Feedback saved", message: "Thanks for sharing")
                }
                return
                
            })
        }

        
    }
    @objc func goBackAction( _sender: UIButton) {
        self.performSegue(withIdentifier: "DogMainCode", sender: self)
    }

}
