//
//  AboutVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 24/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {
    
    let topBarView: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let infoView: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let moveBackToDogMainCode: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "backButton"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goBackAction(_:)), for: .touchUpInside)
        return button
        
    }()
    
    let dogImageProfile: UIImageView = {
        let picture = UIImageView()
        picture.isUserInteractionEnabled = true
        picture.contentMode = .scaleAspectFit
        picture.image = UIImage(named: "Logo Design Dog Schedule -2 Transparent")
        
        return picture
        
    }()
    
    let applicationNameLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Dog Schedule Application"
        label.font = UIFont(name: "GillSans-Bold", size: 17)
        return label
        
    }()
    
    let developerNameLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Alex Grinberg Copyright"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let yearLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "2019"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let copyrightLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "All rights reserved"
        label.font = UIFont(name: "GillSans", size: 14)
        return label
        
    }()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        setupViews()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setupViews() {
        view.addSubview(topBarView)
        topBarView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        
        view.addSubview(moveBackToDogMainCode)
        moveBackToDogMainCode.anchor(top: topBarView.topAnchor, left: topBarView.leftAnchor, bottom: topBarView.bottomAnchor, right: nil, paddingTop: 40, paddingLeft: 20, paddingBottom: 20, paddingRight: 20, width: 30, height: 30)
        
        view.addSubview(dogImageProfile)
        dogImageProfile.anchor(top: topBarView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 180)
        
        view.addSubview(infoView)
        infoView.anchor(top: dogImageProfile.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 50, paddingRight: 40, width: 0, height: 160)
        
        //Stack View for main elements
        let stackView = UIStackView(arrangedSubviews: [applicationNameLabel ,developerNameLabel, yearLabel, copyrightLabel])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        stackView.anchor(top: infoView.topAnchor, left: infoView.leftAnchor, bottom: infoView.bottomAnchor, right: infoView.rightAnchor, paddingTop: 40, paddingLeft: 15, paddingBottom: 15, paddingRight: 15, width: 0, height: 160)
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    

    @objc func goBackAction(_ sender: Any) {
        let vc = DogMainCode()
        self.present(vc, animated: true, completion: nil)
    }

}
