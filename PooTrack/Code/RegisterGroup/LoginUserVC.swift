//
//  LoginUserVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LoginUserVC: UIViewController {
    
    let topBarView: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let profileImageView: UIImageView = {
        let picture = UIImageView()
        picture.isUserInteractionEnabled = true
        picture.contentMode = .scaleAspectFit
        picture.image = UIImage(named: "Logo Design Dog Schedule -2 Transparent")
        
        return picture
        
    }()
    
    let emailTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.keyboardType = .emailAddress
        textfield.placeholder = "Email"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    let passwordTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Password"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()

    let loginButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans", size: 17)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        button.layer.cornerRadius = 5
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowOpacity = 4
        button.borderColor = .white
        button.borderWidth = 1
        button.shadowColor = .white
        return button
        
    }()
    
    let moveToRegisterPage: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Not Register? Move to Register Menu", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans", size: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textColor = .white
        button.backgroundColor = UIColor.rgb(red: 148, green: 195, blue: 85)
        button.addTarget(self, action: #selector(moveToRegisterFunction), for: .touchUpInside)
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowOpacity = 4
        button.layer.cornerRadius = 5
        button.borderColor = .white
        button.borderWidth = 1
        button.shadowColor = .white
        return button
        
    }()
    
    let loginLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Login Menu"
        label.textColor = UIColor.rgb(red: 60, green: 60, blue: 60)
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        setupViews()
        
        
    }
    
    func setupViews() {
        view.addSubview(topBarView)
        topBarView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        
        view.addSubview(profileImageView)
        profileImageView.anchor(top: topBarView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 180)
        
        //Stack View for main elements
        let stackView = UIStackView(arrangedSubviews: [loginLabel, emailTextField, passwordTextField, loginButton, moveToRegisterPage])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        stackView.anchor(top: profileImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 50, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 280)
    }
    
    
    @objc func handleLogin() {
        SVProgressHUD.show()
        
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            print("Form is not valid")
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                print(error ?? "")
                SVProgressHUD.dismiss()
                return
            }
            
            self.dismiss(animated: true, completion: nil)
            let vc = DogMainCode()
            self.present(vc, animated: true, completion: nil)
            SVProgressHUD.dismiss()
            
        })
        
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func moveToRegisterFunction() {
        let vc = RegisterUserVC()
        self.present(vc, animated: true, completion: nil)
    }

}
