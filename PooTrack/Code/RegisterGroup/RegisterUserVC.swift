//
//  RegisterUserVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 08/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class RegisterUserVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    let topBarView: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let profileImageView: UIImageView = {
        let picture = UIImageView()
        picture.isUserInteractionEnabled = true
        picture.contentMode = .scaleAspectFit
        picture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        picture.image = UIImage(named: "profileUpload")
        
        return picture
        
    }()
    
    let dogBonePicture: UIImageView = {
        let picture = UIImageView()
        picture.isUserInteractionEnabled = true
        picture.contentMode = .scaleAspectFit
        picture.image = UIImage(named: "bone")
        
        return picture
        
    }()
    
    
    let choosePicLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Upload your picture"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let nameTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.placeholder = "Name"
        textfield.autocorrectionType = .no
        textfield.keyboardType = .namePhonePad
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
        
    }()
    
    let emailTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.keyboardType = .emailAddress
        textfield.placeholder = "Email"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false

        return textfield
        
    }()
    
    let passwordTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Password"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false

        return textfield
        
    }()
    
    let phoneTextField: UITextField = {
        let textfield = UITextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.placeholder = "Phone"
        textfield.autocorrectionType = .no
        textfield.keyboardType = .phonePad
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect

        return textfield
        
    }()
    
    let saveBtn: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans", size: 17)
        button.addTarget(self, action: #selector(saveUser(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        button.layer.cornerRadius = 5
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowOpacity = 4
        button.borderColor = .white
        button.borderWidth = 1
        button.shadowColor = .white
        return button
        
    }()
    
    let moveToLoginButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Registered? Move to Login Menu", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans", size: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textColor = .white
        button.backgroundColor = UIColor.rgb(red: 148, green: 195, blue: 85)
        button.addTarget(self, action: #selector(moveToLoginFunction), for: .touchUpInside)
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowOpacity = 4
        button.layer.cornerRadius = 5
        button.borderColor = .white
        button.borderWidth = 1
        button.shadowColor = .white
        return button
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        setupVCViews()
        setupDogPawsView()
        
        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        self.profileImageView.isUserInteractionEnabled = true
        self.profileImageView.contentMode = .scaleAspectFit
        self.profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        
        
        SVProgressHUD.dismiss()
    }
    
        @objc func saveUser(_ sender: Any) {
            SVProgressHUD.show()
            handleRegister()
            SVProgressHUD.dismiss()
        
    }
    
    @objc func moveToLoginFunction() {
        let vc = LoginUserVC()
        self.present(vc, animated: true, completion: nil)
    }
    
    func setupDogPawsView() {
        
        view.addSubview(dogBonePicture)
        dogBonePicture.anchor(top: nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 40, paddingRight: 20, width: 50, height: 50)
    }
    
    func setupVCViews() {
        view.addSubview(topBarView)
        topBarView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        
        view.addSubview(profileImageView)
        profileImageView.anchor(top: topBarView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 120)
        
        //Stack View for main elements
        let stackView = UIStackView(arrangedSubviews: [choosePicLabel, nameTextField, emailTextField, passwordTextField, phoneTextField, saveBtn, moveToLoginButton])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        stackView.anchor(top: profileImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 50, paddingLeft: 30, paddingBottom: 100, paddingRight: 30, width: 0, height: 340)
    }

        func handleRegister() {
            guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text, let phone = phoneTextField.text else {
                print("Form is not valid")
                return
            }
            
            Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                
                if error != nil {
                    print(error!)
                    return
                }
                
                guard let uid = user?.user.uid else {
                    return
                }
                
                //successfully authenticated user
                let imageName = UUID().uuidString
                let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
                
                if let profileImage = self.profileImageView.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                    
                    storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in
                        
                        if let error = error {
                            print(error)
                            return
                        }
                        
                        storageRef.downloadURL(completion: { (url, err) in
                            if let err = err {
                                print(err)
                                return
                            }
                            
                            guard let url = url else { return }
                            let values = ["name": name, "email": email, "password": password, "profileImageUrl": url.absoluteString, "mobile": phone, "provider": uid]
                            
                            self.registerUserIntoDatabaseWithUID(uid, values: values as [String : AnyObject])
                            self.newAlert(title: "Your account was created", message: "Enjoy the application")
                            let vc = DogMainCode()
                            self.present(vc, animated: true, completion: nil)
                        })
                        
                    })
                }
            })
        }
        
        fileprivate func registerUserIntoDatabaseWithUID(_ uid: String, values: [String: AnyObject]) {
            let ref = Database.database().reference()
            let usersReference = ref.child("USERS").child(uid).child("Information")
            
            usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                
                if err != nil {
                    print(err!)
                    return
                }
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        @objc func handleSelectProfileImageView() {
            let picker = UIImagePickerController()
            
            picker.delegate = self
            picker.allowsEditing = true
            
            present(picker, animated: true, completion: nil)
        }
        
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            var selectedImageFromPicker: UIImage?
            
            
            if let editedImage = info[UIImagePickerController.InfoKey.editedImage] {
                selectedImageFromPicker = editedImage as? UIImage
            } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage]{
                selectedImageFromPicker = originalImage as? UIImage
            }
            if let selectedImage = selectedImageFromPicker {
                self.profileImageView.image = selectedImage
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            print("cancelled")
            dismiss(animated: true, completion: nil)
        }
    

    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
}

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
}



