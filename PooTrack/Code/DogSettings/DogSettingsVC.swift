//
//  DogSettingsVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 12/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import VirgilCrypto
import VirgilSDK

class DogSettingsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    
    enum CastState {
        case connected
        case disconnected
    }
    
    var castState = CastState.disconnected
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        
        view.layer.masksToBounds = true
        return view
    }()
    
    let topContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var registerDogButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        button.setTitle("Register", for: UIControl.State())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.contentMode = .scaleAspectFit
        button.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 13)
        button.layer.cornerRadius = 5
        button.layer.isOpaque = true
        button.layer.opacity = 0.8
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        
        return button
    }()
    
    @objc func handleSave() {
        handleRegister()
    }
    
    let dogNameTextField: UITextField = {
        let tf = UITextField()
        tf.text = "Dog Name"
        tf.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isOpaque = true
        tf.layer.opacity = 0.8
        tf.layer.masksToBounds = true
        tf.textAlignment = .center
        tf.contentMode = .center
        tf.layer.cornerRadius = 5
        tf.layer.shadowRadius = 2
        tf.layer.shadowOffset = CGSize(width: -1, height: -1)
        tf.font = UIFont(name: "AvenirNext-Regular", size: 12)
        return tf
    }()
    
    let uploadLabel: UILabel = {
        let label = UILabel()
        label.text = "Upload your dog picture"
        label.backgroundColor = UIColor.white
        label.font = UIFont(name: "Avenir-Next-Bold", size: 13)
        label.textAlignment = .center
        label.textColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    let dogNameSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dogAgeTextField: UITextField = {
        let tf = UITextField()
        tf.text = "Dog Age"
        tf.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isOpaque = true
        tf.layer.opacity = 0.8
        tf.layer.masksToBounds = true
        tf.textAlignment = .center
        tf.contentMode = .center
        tf.layer.cornerRadius = 5
        tf.layer.shadowRadius = 2
        tf.layer.shadowOffset = CGSize(width: -1, height: -1)
        tf.font = UIFont(name: "AvenirNext-Regular", size: 12)
        return tf
    }()
    
    let dogAgeSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dogBreedTextField: UITextField = {
        let tf = UITextField()
        tf.text = "Dog Breed"
        tf.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isOpaque = true
        tf.layer.opacity = 0.8
        tf.layer.masksToBounds = true
        tf.textAlignment = .center
        tf.contentMode = .center
        tf.layer.cornerRadius = 5
        tf.layer.shadowRadius = 2
        tf.layer.shadowOffset = CGSize(width: -1, height: -1)
        tf.font = UIFont(name: "AvenirNext-Regular", size: 12)
        
        return tf
    }()
    
    let dogBreedSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    

    lazy var dogProfilePic: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "transparent-White-HQ")
        imageView.backgroundColor = UIColor.rgb(red: 80, green: 101, blue: 161)
        imageView.contentMode = .scaleAspectFit
        
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    let animationImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "transparent-White-HQ")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .blue
        imageView.layer.cornerRadius = 50
        // imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    
    let animationButton: UIButton = {
        let imageView = UIButton()
        imageView.setTitle("click here", for: .normal)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .blue
        imageView.layer.cornerRadius = 50
       //  imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView))
        imageView.addTarget(self, action: #selector(connectAnimation), for: .touchUpInside)
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImageViewAnimation()
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(dogProfilePic)
        dogProfilePic.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 120, paddingLeft: 125, paddingBottom: 0, paddingRight: 0, width: 120, height: 120)
        //dogProfilePic.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.addSubview(uploadLabel)
        uploadLabel.anchor(top: dogProfilePic.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 5, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 30)
        
        view.addSubview(dogNameTextField)
        dogNameTextField.anchor(top: uploadLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 70, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 40)
        
        view.addSubview(dogNameSeparatorView)
        dogNameSeparatorView.anchor(top: dogNameTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 1)
        
        view.addSubview(dogBreedTextField)
        dogBreedTextField.anchor(top: dogNameSeparatorView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 40)
        
        view.addSubview(dogBreedSeparator)
        dogBreedSeparator.anchor(top: dogBreedTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 1)
        
        view.addSubview(dogAgeTextField)
        dogAgeTextField.anchor(top: dogBreedSeparator.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 40)
        
        view.addSubview(dogAgeSeparator)
        dogAgeSeparator.anchor(top: dogAgeTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 8, paddingLeft: 30, paddingBottom: 0, paddingRight: 30, width: 0, height: 1)
        
        view.addSubview(registerDogButton)
        registerDogButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 90, paddingBottom: 250, paddingRight: 90, width: 0, height: 40)
        
        view.addSubview(animationImage)
        animationImage.anchor(top: registerDogButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)

        view.addSubview(animationButton)
        animationButton.anchor(top: animationImage.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)
    }
    
    @objc func connectAnimation() {
        // Disables the button to avoid user interaction when the animation is running
        animationButton.isEnabled = false
        animationImage.startAnimating()
        // Simulates a connection with a delay of 3 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.animationImage.stopAnimating()
            // Enables the button to allow user interaction
            self.animationButton.isEnabled = true
            // Updates UI
            self.toggleCastState()
        }
    }
     func disconnect() {
        // Updates UI
        toggleCastState()
    }
    
    
    func setupImageViewAnimation() {
        animationImage.animationImages = [#imageLiteral(resourceName: "walk-icon-26"), #imageLiteral(resourceName: "dog-4"), #imageLiteral(resourceName: "457135-200")]
        animationImage.animationDuration = 1
    }
    private func toggleCastState() {
        // Updates current Chromecast state
        castState = castState == .connected ? .disconnected : .connected
        // Updates button title
        let buttonTitle = castState == .connected ? "Disconnect" : "Connect"
        animationButton.setTitle(buttonTitle, for: .normal)
        // Updates `UIImageView` default image
        let image = castState == .connected ? #imageLiteral(resourceName: "password") : #imageLiteral(resourceName: "password")
        animationImage.image = image
    }
    
    
    
    
}

