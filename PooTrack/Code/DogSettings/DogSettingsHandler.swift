//
//  DogSettingsHandler.swift
//  PooTrack
//
//  Created by Alex Grinberg on 12/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension DogSettingsVC {
    
    func handleRegister() {
        guard let name = dogNameTextField.text else {return}
        guard let breed = dogBreedTextField.text else {return}
        guard let age = dogAgeTextField.text else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
            
            //successfully authenticated user
            let imageName = UUID().uuidString
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
            
            if let profileImage = self.dogProfilePic.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                
                storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in
                    
                    if let error = err {
                        print(error)
                        return
                    }
                    
                    storageRef.downloadURL(completion: { (url, err) in
                        if let err = err {
                            print(err)
                            return
                        }
                        
                        guard let url = url else { return }
                        let values = ["dog-name": name, "dog-email": breed, "dog-age": age, "profileImageUrl": url.absoluteString, "provider" : uid]
                        
                    
                        let ref = Database.database().reference().child("dogs").child(uid).childByAutoId()
                        ref.updateChildValues(values, withCompletionBlock: {(snapshot, error) in
                            
                            if error != error {
                                print("Working")
                                print(snapshot!)
                            } else {
                                print("Error")
                            }
                            
                            
                        })
                        
                        
                        
                        
                    })
                    
                })
            }
    }
    
    
    fileprivate func registerUserIntoDatabaseWithUID(_ uid: String, values: [String: AnyObject]) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if err != nil {
                print(err!)
                return
            }
            // let user = User(dictionary: values)
            //  self.messagesController?.setupNavBarWithUser(user)
            
            self.dismiss(animated: true, completion: nil)
        })
    }
    

    
    func onConnectButton(_ sender: Any) {
        switch castState {
        case .connected:
            disconnect()
        case .disconnected:
            connectAnimation()
        }
        
    }

    
    
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] {
            selectedImageFromPicker = editedImage as? UIImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage]{
            selectedImageFromPicker = originalImage as? UIImage
        }
        if let selectedImage = selectedImageFromPicker {
            self.dogProfilePic.image = selectedImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}

