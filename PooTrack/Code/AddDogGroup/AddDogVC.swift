//
//  AddDogVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 15/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class AddDogVC: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    let topBarView: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let dogImageProfile: UIImageView = {
        let picture = UIImageView()
        picture.isUserInteractionEnabled = true
        picture.contentMode = .scaleAspectFit
        picture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        picture.image = UIImage(named: "Logo Design Dog Schedule-Face")
        
        return picture
        
    }()
    
    let uploadPicLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Upload your dog picture"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let dognameTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.placeholder = "Name"
        textfield.autocorrectionType = .no
        textfield.keyboardType = .namePhonePad
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
        
    }()
    
    let dogBreedTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.keyboardType = .emailAddress
        textfield.placeholder = "Email"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    let dogWeightTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Password"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    let dogAgeTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Password"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    
    let saveDogInformation: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Save", for: .normal)
        button.titleLabel?.font = UIFont(name: "GillSans", size: 17)
        button.addTarget(self, action: #selector(saveToDB(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        button.layer.cornerRadius = 5
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowOpacity = 4
        button.borderColor = .white
        button.borderWidth = 1
        button.shadowColor = .white
        return button
        
    }()
    
    let moveBackToDogMainCode: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "backButton"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(moveBackFunction), for: .touchUpInside)
        return button
        
    }()
    

    let dogId = Database.database().reference().childByAutoId().key
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        
        SVProgressHUD.show()
        
        self.dogImageProfile.isUserInteractionEnabled = true
        self.dogImageProfile.contentMode = .scaleAspectFit
        self.dogImageProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        
        SVProgressHUD.dismiss()
        setupViews()

    }
    
    func setupViews() {
        view.addSubview(topBarView)
        topBarView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        
        view.addSubview(moveBackToDogMainCode)
        moveBackToDogMainCode.anchor(top: topBarView.topAnchor, left: topBarView.leftAnchor, bottom: topBarView.bottomAnchor, right: nil, paddingTop: 40, paddingLeft: 20, paddingBottom: 20, paddingRight: 20, width: 30, height: 30)
        
        view.addSubview(dogImageProfile)
        dogImageProfile.anchor(top: topBarView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 120)
        
        //Stack View for main elements
        let stackView = UIStackView(arrangedSubviews: [uploadPicLabel ,dognameTextField, dogBreedTextField, dogAgeTextField, dogWeightTextField, saveDogInformation])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        stackView.anchor(top: dogImageProfile.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 50, paddingLeft: 30, paddingBottom: 100, paddingRight: 30, width: 0, height: 340)
    }
    
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] {
            selectedImageFromPicker = editedImage as? UIImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage]{
            selectedImageFromPicker = originalImage as? UIImage
        }
        if let selectedImage = selectedImageFromPicker {
            self.dogImageProfile.image = selectedImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    @objc func saveToDB(_ sender: UIButton) {
        SVProgressHUD.show()
        handleRegister()
        SVProgressHUD.dismiss()
    }
    
    fileprivate func registerDog(_ uid: String, values: [String: AnyObject]) {
        
        let ref = Database.database().reference()
        let usersReference = ref.child("USERS").child(uid).child("Dogs").child(dogId!)
            usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if err != nil {
                print(err!)
                return
            }
        })
    }
    
    
    func handleRegister() {
        guard let name = dognameTextField.text, let breed = dogBreedTextField.text, let age = dogAgeTextField.textColor, let weight = dogWeightTextField.text else {
            print("Form is not valid")
            return
        }
                guard let uid = Auth.auth().currentUser?.uid else {
                return
            }
            //successfully authenticated user
            let imageName = UUID().uuidString
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
            
            if let profileImage = self.dogImageProfile.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                
                storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in
                    
                    if let error = err {
                        print(error)
                        return
                    }
                    
                    storageRef.downloadURL(completion: { (url, err) in
                        if let err = err {
                            print(err)
                            return
                        }
                        
                        guard let url = url else { return }
                        let values = ["dogname": name, "dogbreed": breed, "birthday": age, "profileImageUrl": url.absoluteString, "weight":weight, "doguid": self.dogId as Any] as [String : Any]
                        self.registerDog(uid, values: values as [String: AnyObject])
                       // self.registerUserIntoDatabaseWithUID(uid, values: values as [String : AnyObject])
                    })
                    
                })
            }
        }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
     @objc func moveBackFunction() {
        let vc = DogMainCode()
        self.present(vc, animated: true, completion: nil)
    }
}
