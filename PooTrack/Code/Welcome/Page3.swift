//
//  Page3.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class Page3: UIViewController {
        
        let titleLabel: UILabel = {
            let title = UILabel()
            title.text = "Enjoy the application!"
            title.font = UIFont(name: "GillSans-Bold", size: 50)
            title.textAlignment = .center
            title.textColor = .white
            
            return title
        }()
        
        let subTitle: UILabel = {
            
            let subtitle = UILabel()
            subtitle.text = "Save walks, where and how much times your dog pee or poo."
            subtitle.numberOfLines = 5
            subtitle.textColor = .white
            subtitle.contentMode = .center
            subtitle.textAlignment = .center
            subtitle.font = UIFont(name: "GillSans", size: 25)
            
            return subtitle
            
        }()
        
        let logoImage: UIImageView = {
            
            let logo = UIImageView()
            let image = UIImage(named: "Logo Design Dog Schedule-Face")
            logo.image = image
            logo.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
            logo.contentMode = .scaleAspectFit
            
            return logo
            
        }()
    
    let saveButton: UIButton = {
        
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setTitle("Start", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        return button
        
    }()
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.view.backgroundColor = UIColor.rgb(red: 140, green: 205, blue: 209)
            
            setupView()
            
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
    func setupView() {
        
        let stackView = UIStackView(arrangedSubviews: [logoImage, titleLabel, subTitle])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .equalSpacing
        
        view.addSubview(stackView)
        stackView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 30, paddingBottom: 100, paddingRight: 30, width: 0, height: 400)
        
    }
        
        
}

