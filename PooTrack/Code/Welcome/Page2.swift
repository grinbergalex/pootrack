//
//  Page2.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class Page2: UIViewController {
    
    let titleLabel: UILabel = {
        let title = UILabel()
        title.text = "Register your account"
        title.font = UIFont(name: "GillSans-Bold", size: 25)
        title.textAlignment = .center
        title.textColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        
        return title
    }()
    
    let subTitle: UILabel = {
        
        let subtitle = UILabel()
        subtitle.text = "Create an user account\nUse your email to register"
        subtitle.numberOfLines = 2
        subtitle.textColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        subtitle.contentMode = .scaleAspectFit
        subtitle.textAlignment = .center
        subtitle.font = UIFont(name: "GillSans", size: 22)
        
        return subtitle
        
    }()
    
    let logoImage: UIImageView = {
        
        let logo = UIImageView()
        let image = UIImage(named: "dogOwner")
        logo.image = image
        logo.contentMode = .scaleAspectFit
        return logo
        
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        setupView()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupView() {
        
        let stackView = UIStackView(arrangedSubviews: [logoImage, titleLabel, subTitle])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .equalSpacing
        
        view.addSubview(stackView)
        stackView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 80, paddingLeft: 30, paddingBottom: 100, paddingRight: 30, width: 0, height: 400)
        
    }
    
    
}

