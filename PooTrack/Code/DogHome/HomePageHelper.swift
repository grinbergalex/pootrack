//
//  HomePageHelper.swift
//  PooTrack
//
//  Created by Alex Grinberg on 18/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MapKit
import UserNotifications


extension DogMainCode {
    
    class customPin: NSObject, MKAnnotation {
        var coordinate: CLLocationCoordinate2D
        var title: String?
        var subtitle: String?
        
        init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
            self.title = pinTitle
            self.subtitle = pinSubTitle
            self.coordinate = location
        }
    }
    
    func getDirections() {
        
        let userlocation = MKUserLocation()
        
        let sourceLocation = CLLocationCoordinate2D(latitude:userlocation.coordinate.latitude , longitude: userlocation.coordinate.longitude)
        let destinationLocation = CLLocationCoordinate2D(latitude:38.643172 , longitude: -90.177429)
        
        let sourcePin = customPin(pinTitle: "Kansas City", pinSubTitle: "", location: sourceLocation)
        let destinationPin = customPin(pinTitle: "St. Louis", pinSubTitle: "", location: destinationLocation)
        self.mapView.addAnnotation(sourcePin)
        self.mapView.addAnnotation(destinationPin)
        
        let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionRequest.transportType = .walking
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    print("we have error getting directions==\(error.localizedDescription)")
                }
                return
            }
            
            let route = directionResonse.routes[0]
            self.mapView.addOverlay(route.polyline, level: .aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
        
        self.mapView.delegate = self
        
        
    }
    
    
    
////    func newAlert(title: String, message: String) {
////        
////        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
////        
////        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
////        
////        self.present(alertController, animated: true, completion: nil)
////        
////    }
////    func newAlertCancel(title: String, message: String) {
////        
////        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
////        
////        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
////        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
////        
////        
////        self.present(alertController, animated: true, completion: nil)
////        
//    }

}

