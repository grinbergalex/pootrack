//
//  ViewController.swift
//  PooTrack
//
//  Created by Alex Grinberg on 06/03/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import CoreLocation

class DogMainCode: UIViewController, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
   
    var userPic = [USERINFO]()
    
    //Objects of UIX:
    //MapView:
    let mapView: MKMapView = {
       let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
        
    }()
    
    let topBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let username: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.text = "Select which dog you are walking"
        label.font = UIFont(name: "Avenir-Next", size: 14)
        
        return label
        
    }()
    
    let backgroundLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.text = "Select which dog you are walking"
        label.font = UIFont(name: "Avenir-Next", size: 14)

        return label
        
    }()
    
    //Bottom View, controls of the walk:
    let bottomPanelView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    //Side Menu Button
    let userTopImage: UIImageView = {
        let picture = UIImageView()
        picture.image = UIImage(named: "Logo Design Dog Schedule-Face")
        picture.translatesAutoresizingMaskIntoConstraints = false
        picture.layer.cornerRadius = 12
        picture.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        picture.clipsToBounds = true
        picture.layer.borderWidth = 1.0
        picture.layer.borderColor = UIColor.white.cgColor
        picture.contentMode = .scaleAspectFit
        return picture
        
    }()
    
    //Add Dog Button
    let addDogbutton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 40
        button.backgroundColor = .black
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.red.cgColor
        button.layer.shadowOffset = CGSize(width: 4, height: 2)
        button.setTitle("+", for: .normal)
        button.titleLabel?.font = UIFont(name: "Avenir", size: 50)
        button.addTarget(self, action: #selector(addNewDog(_:)), for: .touchUpInside)
        return button
        
    }()
    
    //Add Dog Button
    let cancelDogWalk: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 40
        button.backgroundColor = .black
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.red.cgColor
        button.layer.shadowOffset = CGSize(width: 4, height: 2)
        button.setTitle("x", for: .normal)
        button.titleLabel?.font = UIFont(name: "Avenir", size: 50)
        button.addTarget(self, action: #selector(addNewDog(_:)), for: .touchUpInside)
        return button
        
    }()
    
    //Center User Button
    let centerUserButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "menu-2"), for: .normal)
        button.addTarget(self, action: #selector(centerBtnWasPressed), for: .touchUpInside)
        return button
        
    }()
    
    //Pee Button:
    let peeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 15
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = .white
        button.setImage(UIImage(named: "pee2"), for: .normal)
        button.addTarget(self, action: #selector(peeDone(_:)), for: .touchUpInside)
        return button
        
    }()
    //Poo button:
    let pooButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "poo"), for: .normal)
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(pooDone(_:)), for: .touchUpInside)
        return button
        
    }()
    
    //Refresh Button:
    let refreshButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "refresh"), for: .normal)
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(refreshWalk), for: .touchUpInside)
        return button
        
    }()
    
    //Start Walking button:
    let startWalkBtn: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .red
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(dogWalkStarted(_:)), for: .touchUpInside)
        return button
        
    }()
    
    let settingsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(), for: .touchUpInside)
        return button
        
    }()
    
    let stopWalkBtn: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .blue
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(stopWalking(_:)), for: .touchUpInside)
        return button
        
    }()
    
    let containerView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 250, green: 250, blue: 250)
        return view
        
    }()
    
    let distanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Next", size: 15)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Distance"
        label.textColor = UIColor.rgb(red: 50, green: 50, blue: 50)
        label.numberOfLines = 2
        return label
        
    }()
    
    let durationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Next", size: 15)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Duration"
        label.textColor = UIColor.rgb(red: 50, green: 50, blue: 50)
        label.numberOfLines = 2
        return label
        
    }()
    
    let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .singleLine
        table.backgroundColor = .clear
        table.clipsToBounds = true
        table.layer.cornerRadius = 40
        table.cornerRadius = 40
        
        return table
        
    }()
    
    let dogView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
        
    }()
    
    let backgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        view.isOpaque = true
        view.layer.opacity = 0.8
        view.tag = 800
        
        return view
        
    }()
    
    let selectDogsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Please select the walk you are walking or cancel"
        label.font = UIFont(name: "Avenir", size: 17)
        label.numberOfLines = 2
        label.contentMode = .center
        label.textAlignment = .center
        label.backgroundColor = .clear
        
        return label
        
    }()
    
    let resultView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
        
    }()
    let durationResult: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    let distanceResult: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    let saveResults: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Save", for: .normal)
        return button
        
    }()
    
    let dogPofilePic: UIImageView = {
        let dogImage = UIImageView()
        dogImage.translatesAutoresizingMaskIntoConstraints = false
        return dogImage
        
    }()
    
    var resultStackView: UIStackView!
    
    var userOptions = [AvailableDogs]()
    var users = [USERINFO]()
    
    var dogSelectedId = ""
    
    var integer: Int = 0
    
    var updatedDistance: NSString = ""
    
    var dogWasChecked = true
    
    var buttonSwitched : Bool = false
    
    var timeCount: Int = 0
    var distanceCount: NSString!
    var timer: Timer!
    
    var userLastLocation: CLLocation!
    
    var startUserLocation: CLLocation!
    var finalUserLocation: CLLocation!
    var startDate: Date!
    var traveledDistance: String = ""
    
    var distance: Double = 0
    
    var distanceTravelled: NSString!
    var isShow = false
    
    var userDogs = [UserDogsModel]()
    
    var DOG = [DogModel]()
    
    var userLocation: MKUserLocation!
    
    var manager: CLLocationManager!
    var regionRadius: CLLocationDistance = 1000
    
    var currentLocation: CLLocation! = nil
    
    
    var lastLocation = [CLLocationCoordinate2D]()
    
    var uid = Auth.auth().currentUser?.uid
    
    let dogUID = Database.database().reference().childByAutoId().key
    
    var walkUID = Database.database().reference().childByAutoId().key
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        backgroundLabel.isHidden = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(AvailableDogsCell.self, forCellReuseIdentifier: "AvailableDogsCell")
        self.tableView.isHidden = true

        //Start the main button disabled:
        self.peeButton.isEnabled = false
        self.peeButton.layer.opacity = 0.5
        
        self.pooButton.isEnabled = false
        self.pooButton.layer.opacity = 0.5
        
        self.refreshButton.isEnabled = false
        self.refreshButton.layer.opacity = 0.5
        
        fetchUserDogs()
        fetchUserInfo()
        
        self.addDogbutton.isHidden = true
        self.dogView.isHidden = true
        
        self.manager = CLLocationManager()
        self.mapView.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        self.mapView.mapType = MKMapType(rawValue: 0)!
        self.checkLocationStatus()
        self.centerMapOnUserLocation()
        self.centerBtnWasPressed()
        self.isShow = false
        self.stopWalkBtn.isHidden = true
        
        setupTopBarView()
        setupBottomView()
        setupInputFields()
        
        startWalkBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        startWalkBtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
    }
    
    
    func setupTopBarView() {
        
        //MapView object - all the screen
        view.addSubview(mapView)
        mapView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        mapView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mapView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        //TopBar View:
        view.addSubview(topBarView)
        topBarView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 80)
        //Side menu button:
        view.addSubview(userTopImage)
        userTopImage.anchor(top: view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: 45, paddingLeft: 0, paddingBottom: 0, paddingRight: 15, width: 70, height: 70)

        //Pee Button:
        view.addSubview(peeButton)
        peeButton.anchor(top: topBarView.topAnchor, left: view.leftAnchor, bottom: topBarView.bottomAnchor, right: nil, paddingTop: 45, paddingLeft: 120, paddingBottom: 20, paddingRight: 0, width: 35, height: 35)

        //Poo button:
        view.addSubview(pooButton)
        pooButton.anchor(top: topBarView.topAnchor, left: peeButton.rightAnchor, bottom: topBarView.bottomAnchor, right: nil, paddingTop: 45, paddingLeft: 10, paddingBottom: 20, paddingRight: 0, width: 30, height: 30)

        //Refresh Button: Option to re-start the walk
        view.addSubview(refreshButton)
        refreshButton.anchor(top: topBarView.topAnchor, left: pooButton.rightAnchor, bottom: topBarView.bottomAnchor, right: nil, paddingTop: 45, paddingLeft: 10, paddingBottom: 20, paddingRight: 15, width: 30, height: 30)

    }
    
    func setupBottomView() {
        
        //Bottom Panel:
        view.addSubview(bottomPanelView)
        bottomPanelView.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100)
    }
    
    func setupDogView() {
        
        view.addSubview(addDogbutton)
        addDogbutton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: 100, paddingLeft: 0, paddingBottom: 5, paddingRight: 10, width: 80, height: 80)
        
        view.addSubview(tableView)
        tableView.anchor(top: addDogbutton.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 100, paddingRight: 10, width: 80, height: 300)
    }
    
    fileprivate func setupInputFields() {
     let stackView = UIStackView(arrangedSubviews: [distanceLabel, stopWalkBtn ,startWalkBtn, durationLabel])
     
     stackView.axis = .horizontal
     stackView.spacing = 10
     stackView.distribution = .fillEqually
     
     view.addSubview(stackView)
     stackView.anchor(top: bottomPanelView.topAnchor, left: bottomPanelView.leftAnchor, bottom: bottomPanelView.bottomAnchor, right: bottomPanelView.rightAnchor, paddingTop: 30, paddingLeft: 15, paddingBottom: 20, paddingRight: 15, width: 0, height: 250)
     }
    
    
    @objc func refreshWalk() {
        
        self.newWalkAlert(title: "Are you sure you want to restart your progress?", message: "Current data won't be saved")
        
        
    }
    
    @objc func 
    
    func fetchUserDog() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let childId = Database.database().reference().child("USERS").child(uid).child("Walks").child(walkUID!)
        childId.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = UserDogsModel()
                index.dogUrlImage = ((dictionary["profileImageUrl"]) as? String)!
                
                // MARK: Some issues were found here!!!!!
                //   index.dogName = ((dictionary["dogname"]) as? String)!
                
                self.userDogs.append(index)
                
                let url = URLRequest(url: URL(string: index.dogUrlImage)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.dogPofilePic.image = UIImage(data: data!)
                    }
                }
                
                task.resume()
            }
            print(snapshot)
            
        }, withCancel: nil)
        
    }
    
    func checkLocationStatus() {
        manager = CLLocationManager()
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
        } else {
            manager.requestWhenInUseAuthorization()
            
        }
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
        }
        
    }
    @objc func addNewDog(_ sender: UIButton) {
        let vc = AddDogVC()
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func cancelWalkDog(_ sender: UIButton) {
        backgroundView.isHidden = true
        addDogbutton.isHidden = true
        tableView.isHidden = true
    }
    
    func centerMapOnUserLocation() {
        
        let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2)
        mapView.setRegion(coordinateRegion, animated: true)
        
        //  print(mapView.userLocation.coordinate)
        
    }

    @objc func centerBtnWasPressed() {
        centerMapOnUserLocation()
        
    }
    
    
    @objc func stopWalking(_ sender: Any) {
        
        
        fetchUserDog()
        
        self.manager.stopUpdatingLocation()
        self.mapView.showsUserLocation = false
        self.mapView.userTrackingMode = .none
        self.tableView.isHidden = true
        self.addDogbutton.isHidden = true
        self.startWalkBtn.isHidden = true
        self.resultView.isHidden = false
        self.distanceResult.text = distanceLabel.text
        self.durationResult.text = durationLabel.text
        
        //Fetch data for dog table view:
        fetchUserDogs()
        
        //Start the main button disabled:
        self.peeButton.isEnabled = false
        self.peeButton.layer.opacity = 0.5
        
        self.pooButton.isEnabled = false
        self.pooButton.layer.opacity = 0.5
        
        self.refreshButton.isEnabled = false
        self.refreshButton.layer.opacity = 0.5
        
        
        
        
        
        
    }
    
    func saveWalkBtn(_ sender: UIButton) {
        
        self.dogWasChecked = true
        
        let ref = Database.database().reference().child(uid!).child("Dogs")
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let dogs = AvailableDogs()
                dogs.dogURL = (dictionary["profileImageUrl"] as! String?)!
                dogs.uid = (dictionary["doguid"] as! String?)!
                self.userOptions.append(dogs)
                self.dogSelectedId = dogs.uid
                let values = ["dogId": self.walkUID!, "duration": self.durationLabel.text!, "distance": self.distanceLabel.text!, "isDogsAvailable": self.dogWasChecked] as [String : Any]
                
                let ref = Database.database().reference().child("USERS").child(self.uid!).child("Walks").child(self.walkUID!)
                ref.updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(error, ref) in
                    
                    if error != nil {
                        print("Worked")
                        //MARK: Need to make sure this works.
                        self.manager = CLLocationManager()
                        self.mapView.delegate = self
                        self.manager.desiredAccuracy = kCLLocationAccuracyBest
                        self.mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
                        self.mapView.mapType = MKMapType(rawValue: 0)!
                        self.checkLocationStatus()
                        self.centerMapOnUserLocation()
                        self.centerBtnWasPressed()
                        self.isShow = false
                        
                        
                    } else {
                        print("Didn't work")
                    }
                    
                    
                })
                
            }
        })
        
    }
    
    func fetchUserInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let ref = Database.database().reference().child("USERS").child(uid).child("Information")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = USERINFO()
                index.profileImageUrl = ((dictionary["profileImageUrl"]) as? String)!
                self.users.append(index)
                
                let url = URLRequest(url: URL(string: index.profileImageUrl)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.userTopImage.image = UIImage(data: data!)
                    }
                    
                }
                
                task.resume()
                
                
            }
            print(snapshot)
            
        }, withCancel: nil)
        
    }
    
    func loadDogPicForResult() {
        
        let ref = Database.database().reference().child(uid!).child("Walks").child(walkUID!)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let dogs = AvailableDogs()
                dogs.dogURL = (dictionary["profileImageUrl"] as! String?)!
                dogs.uid = (dictionary["doguid"] as! String?)!
                self.userOptions.append(dogs)
                self.dogSelectedId = dogs.uid
                let values = ["duration": self.durationLabel.text!, "distance": self.distanceLabel.text!, "isDogsAvailable": self.dogWasChecked] as [String : Any]
                
                let ref = Database.database().reference().child("USERS").child(self.uid!).child("Walks").child(self.walkUID!)
                ref.updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(error, ref) in
                    
                    if error != nil {
                        print("Worked")
                    } else {
                        print("Didn't work")
                    }
                    
                    
                })
                
            }
        })
        
    }
    
    @objc func dogWalkStarted(_ sender: Any) {
        self.backgroundLabel.isHidden = false
        self.tableView.isHidden = false
        self.dogWasChecked = true
        self.addDogbutton.isHidden = false
        self.startWalkBtn.isEnabled = false
        self.stopWalkBtn.isHidden = false
        
        self.peeButton.isEnabled = true
        self.peeButton.layer.opacity = 1
        
        self.pooButton.isEnabled = true
        self.pooButton.layer.opacity = 1
        
        self.refreshButton.isEnabled = true
        self.refreshButton.layer.opacity = 1
        
        
        view.addSubview(backgroundView)
        backgroundView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        view.addSubview(selectDogsLabel)
        selectDogsLabel.anchor(top: backgroundView.topAnchor, left: backgroundView.leftAnchor, bottom: backgroundView.bottomAnchor, right: backgroundView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        setupDogView()
        fetchUserDogs()
        
        backgroundLabel.isHidden = true
        
        
        
    }
    
    func fetchUserDogs() {
        let uid = Auth.auth().currentUser?.uid
        let DB_BASE = Database.database().reference().child("USERS").child(uid!).child("Dogs")
        DB_BASE.observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let dogs = AvailableDogs()
                dogs.dogURL = (dictionary["profileImageUrl"] as! String?)!
                dogs.uid = (dictionary["doguid"] as! String?)!
                dogs.dogName = (dictionary["dogname"] as! String?)!
                self.userOptions.append(dogs)
                
                if snapshot.value != nil {
                    let url = URLRequest(url: URL(string: dogs.dogURL)!)
                    
                    let task = URLSession.shared.dataTask(with: url) {
                        (data, response, error) in
                        if error != nil {
                            print(error!)
                            return
                        }
                    }
                    task.resume()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        self.tableView.reloadData()
                    }
                } else {
                    print("No Data")
                }
            }
            print(snapshot)
            
        }, withCancel: nil)
        
    }
    
    func fetchUserPicture() {
            let uid = Auth.auth().currentUser?.uid
            let DB_BASE = Database.database().reference().child("USERS").child(uid!)
            DB_BASE.observe(.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let dogs = AvailableDogs()
                    dogs.dogURL = (dictionary["profileImageUrl"] as! String?)!
                    dogs.uid = (dictionary["doguid"] as! String?)!
                    self.userOptions.append(dogs)
                    
                    if snapshot.value != nil {
                        let url = URLRequest(url: URL(string: dogs.dogURL)!)
                        
                        let task = URLSession.shared.dataTask(with: url) {
                            (data, response, error) in
                            if error != nil {
                                print(error!)
                                return
                            }
                        }
                        task.resume()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            self.tableView.reloadData()
                        }
                    } else {
                        print("No Data")
                    }
                }
                print(snapshot)
                
            }, withCancel: nil)
        
    }
    
    
    func directions() {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 37.783333, longitude: -122.416667), addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .transit
        
        let directions = MKDirections(request: request)
        print(request.source!)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    func createPin(title: String, subtitle: String) {
        let newPin = MKPointAnnotation()
        
        newPin.coordinate = mapView.userLocation.coordinate
        newPin.title = title
        newPin.subtitle = subtitle
        mapView.addAnnotation(newPin)
        
    }
    
    func getLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        let coordinate = CLLocationCoordinate2D()
        let lat = coordinate.latitude
        let long = coordinate.latitude
        
        print(lat)
        print(long)
        
        
    }
    
    @objc func pooDone(_ sender: UIButton) {
        
        self.createPin(title: "Poo Spot!", subtitle: "Your dog poo here")
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MM yyyy HH:mm:ss"
        let result = formatter.string(from: date)
        
        let peeid = Database.database().reference().childByAutoId().key
        
        let values = ["pooId": peeid!, "date": result] as [String : Any]
        
        let ref = Database.database().reference().child("USERS").child(uid!).child("Walks").child(walkUID!)
        ref.updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("Error")
            } else {
                print("Success")
                
                
            }
        })
        
    }
    
    
    
    
    @objc func peeDone(_ sender: UIButton) {
        
        self.createPin(title: "Pee Spot!", subtitle: "Your dog peeded here")
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MM yyyy HH:mm:ss"
        let result = formatter.string(from: date)
        
        let peeid = Database.database().reference().childByAutoId().key
        
        let values = ["peeId": peeid!, "date": result] as [String : Any]
        
        let ref = Database.database().reference().child(uid!).child("Walks").child(walkUID!)
        ref.updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("Error")
            } else {
                print("Success")
                
                
            }
        })
        
    }
    //Updating every second the distance walked
    @objc func distanceRunning() {
        
        timeCount += 1
        self.distanceLabel.text = distanceTravelled as String?
        self.distanceResult.text = distanceTravelled as String?
        
    }
    
    //MARK: Need to understand why the distance label is not being updated.
    //Every second the value should be updated
    @objc func timerDidFire() {
        timeCount += 1
        
        self.durationLabel.text = NSString(format: "%02d:%02d:%02d", timeCount/3600,(timeCount/60)%60,timeCount%60) as String
    }
    
    @objc func distanceLabelUpdate() {
        
        if distanceTravelled == "" {
            distanceTravelled = "0"
        } else {
            durationLabel.text = traveledDistance
        }
    }
    func getDirections(_ sender: UIButton) {
        
        let latitude:CLLocationDegrees =  37.38
        let longitude:CLLocationDegrees =  -127.66
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Test"
        mapItem.openInMaps(launchOptions: options)
        
    }
    //Function related to the PolyLine of the MAP User Walk:
    //This is responsible to update the line on the user directions
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations.last ?? "none")
        
        
        
        let location = locations.last! as CLLocation
        
        if lastLocation.count == 0 {
            
            if isShow == false {
                lastLocation.append(CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude))
            }
            
        }
        else {
            //If the start Walk button is started
            if isShow == true {
                
                if (lastLocation.last?.latitude != location.coordinate.latitude) && (lastLocation.last?.longitude != location.coordinate.longitude) {
                    
                    lastLocation.append(CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude))
                    let polyline = MKPolyline(coordinates: lastLocation, count: lastLocation.count)
                    mapView.addOverlay(polyline)
                    
                    print(locations.last ?? "none")
                    if startDate == nil {
                        startDate = Date()
                    } else {
                        print("elapsedTime:", String(format: "%.0fs", Date().timeIntervalSince(startDate)))
                    }
                    if startUserLocation == nil {
                        startUserLocation = locations.first
                    } else if let location = locations.last {
                        self.traveledDistance += String(userLastLocation.distance(from: location))
                        print("Traveled Distance:",  traveledDistance)
                        print("Straight Distance:", startUserLocation.distance(from: locations.last!))
                        let finalDistance = startUserLocation.distance(from: locations.last!)
                        integer = (traveledDistance as NSString).integerValue
                        
                        distanceLabel.text = String(finalDistance)
                    }
                    userLastLocation = locations.last
                    
                }
            }
        }
        
        //self.mapView.setRegion(region, animated: true)
        self.mapView.showsUserLocation = true
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        lastLocation.append(CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude))
        let polyline = MKPolyline(coordinates: lastLocation, count: lastLocation.count)
        mapView.addOverlay(polyline)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.red
            pr.lineWidth = 5
            return pr
        }
        return MKOverlayRenderer()
        
    }
    
    
    @objc func appSettings(_ sender: Any) {
        
 //       self.delegate?.toggleLeftPanelVC()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableDogsCell", for: indexPath) as! AvailableDogsCell
        cell.backgroundColor = .clear
        
        let index = userOptions[indexPath.row].dogURL
//        let dogname = userOptions[indexPath.row].dogName
 //       cell.timeLabel.text = dogname
        
        let url = URLRequest(url: URL(string: index)!)
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                cell.profileImageView.image = UIImage(data: data!)
            }
            
        }
        
        task.resume()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        self.timer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.timerDidFire), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
        self.tableView.isHidden = true
        self.addDogbutton.isHidden = true
        self.startWalkBtn.isHidden = true
        
        self.isShow = true
        self.startWalkBtn.isEnabled = false
        self.dogWasChecked = true
        self.backgroundLabel.isHidden = true
        self.selectDogsLabel.isHidden = true
        self.backgroundView.isHidden = true
        
        
        let index = userOptions[indexPath.row].dogURL
        let values = ["dogId": self.walkUID!, "duration": self.durationLabel.text!, "distance": self.distanceLabel.text!, "isDogsAvailable": self.dogWasChecked, "profileImageUrl": index] as [String : Any]
        let ref = Database.database().reference().child("USERS").child(uid!)
        ref.child("Walks").child(walkUID!).updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("Worked, walk saved")
            } else {
                print("error")
            }
            
        })
    }
    
    
    func newWalkAlert(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            UIAlertAction in
            self.dismiss(animated: true, completion: nil)
            self.isShow = false
            self.startWalkBtn.isSelected = false
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        if touch.view?.tag == 800 {
            backgroundView.isHidden = true
            tableView.isHidden = true
            addDogbutton.isHidden = true
            stopWalking(self)
        }
    
    }
    
}

extension DogMainCode: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
        if status == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }

}
