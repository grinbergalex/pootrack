//
//  LocationHelpers.swift
//  PooTrack
//
//  Created by Alex Grinberg on 12/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class LOCATIONS: NSObject {
    
    var latitude: String!
    var longitude: String!
}

class UID {
    var uid: String! = nil
    var longitude: String! = nil
    var latitude: String! = nil
}
