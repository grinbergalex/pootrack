//
//  UserDogsModel.swift
//  PooTrack
//
//  Created by Alex Grinberg on 17/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

class UserDogsModel: NSObject {
    
    var dogName = ""
    var dogImage =  UIImageView()
    var dogUrlImage = ""
    var id = ""
    
}

class DogModel: NSObject {
    var id: String?
    var name: String?
    var image: UIImageView?
    var profileImageUrl: String?
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? String
        self.name = dictionary["dogName"] as? String
        self.image?.image = dictionary["dogImageUrl"] as? UIImage
        self.profileImageUrl = dictionary["profileImageUrl"] as? String
    }
}

class UserHomeInfo: NSObject {
    var username = ""
    var userPic = UIImageView()
    var userUrl = ""
    var userEmail = ""
    
    
}
