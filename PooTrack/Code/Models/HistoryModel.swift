//
//  HistoryModel.swift
//  PooTrack
//
//  Created by Alex Grinberg on 11/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

class HistoryModel: NSObject {
    
    var weekDay = ""
    var duration = ""
    var distance = ""
    var name = ""
    var image = UIImageView()
    var imageUrl = ""
    var dogUid = ""
    
}
