//
//  IMAGES.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class IMAGES: NSObject {
    var dogImageView: UIImageView!
}

class USERINFO: NSObject {
    var name = ""
    var email = ""
    var phone = ""
    var profileImageUrl = ""
    var password = ""
    var profileImage = UIImageView()
}

class DOGBASICINFO: NSObject {
    var dogName = ""
    var dogKmsString = ""
    var generalPooInfoString = ""
    var generalPeeInfoString = ""
    var generalCellImage: UIImageView!
    
}
