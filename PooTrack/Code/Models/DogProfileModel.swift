//
//  DogProfileModel.swift
//  PooTrack
//
//  Created by Alex Grinberg on 11/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

class DogProfile: NSObject {
    
    var dogName = ""
    var dogImage: UIImage!
    
}

class AvailableDogs: NSObject {
    
    var dogURL = ""
    var dogImage: UIImage!
    var uid = ""
    var dogName = ""
    
}
