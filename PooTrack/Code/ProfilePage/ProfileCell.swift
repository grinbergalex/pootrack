//
//  ProfileCell.swift
//  PooTrack
//
//  Created by Alex Grinberg on 12/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class ProductCell : UITableViewCell {
        
        
    let dogNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont(name: "AvenirNext-Bold", size: 16)
        lbl.textAlignment = .center
    return lbl
            
    }()
        
        let dogImagePicture : UIImageView = {
            let imgView = UIImageView()
        imgView.image = UIImage(named: "")
        imgView.contentMode = .scaleAspectFit
            imgView.clipsToBounds = true
            imgView.translatesAutoresizingMaskIntoConstraints = false
            return imgView
        }()
        
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            addSubview(dogImagePicture)
            dogImagePicture.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 30, paddingRight: 0, width: 0, height: 70)
            dogImagePicture.backgroundColor = .blue
            
            addSubview(dogNameLabel)
            dogNameLabel.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 30)
            dogNameLabel.backgroundColor = .yellow
            
            
            
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
