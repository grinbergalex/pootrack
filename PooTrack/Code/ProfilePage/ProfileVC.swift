//
//  ProfileVC.swift
//  PooTrack
//
//  Created by Alex Grinberg on 08/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class ProfileVC: UIViewController, UITextFieldDelegate {
    
    
    let nameTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.placeholder = "Name"
        textfield.autocorrectionType = .no
        textfield.keyboardType = .namePhonePad
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
        
    }()
    
    let emailTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.keyboardType = .emailAddress
        textfield.placeholder = "Email"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    let passwordTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Password"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()
    
    let phoneTextField: UITextField = {
        let textfield = UITextField()
        textfield.contentMode = .scaleAspectFit
        textfield.textAlignment = .left
        textfield.autocorrectionType = .no
        textfield.keyboardType = .emailAddress
        textfield.placeholder = "Email"
        textfield.font = UIFont(name: "GillSans", size: 17)
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        return textfield
        
    }()

    
    @IBOutlet weak var userBackgroundImage: UIImageView!
    @IBOutlet weak var userProfilePic: UIImageView!
    
    @IBOutlet weak var backTopBarButton: UIButton!
    
    var userinfoArray = [USERINFO]()

        override func viewDidLoad() {
            super.viewDidLoad()

            for family in UIFont.familyNames {
                
                let sName: String = family as String
                print("family: \(sName)")
                
                for name in UIFont.fontNames(forFamilyName: sName) {
                    print("name: \(name as String)")
                }
            }
            
            view.backgroundColor = UIColor.white
            
            SVProgressHUD.show()
            fetchUserInfo()
            SVProgressHUD.dismiss()
    }

    
    func newDogCreation(_ sender: UIButton) {
        
        SVProgressHUD.show()
        
        let vc = DogSettingsVC()
        self.present(vc, animated: true, completion: nil)
        
        SVProgressHUD.dismiss()
        
        
    }

    @objc func coOwnerCreation() {
        
        SVProgressHUD.show()
        
        let vc = NewOwnnersVC()
        self.present(vc, animated: true, completion: nil)
        
        SVProgressHUD.dismiss()
 
    }
    
    func fetchUserInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        let ref = Database.database().reference().child("USERS").childByAutoId().child(uid)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = USERINFO()
                index.profileImageUrl = ((dictionary["profileImageUrl"]) as? String)!
                index.name = ((dictionary["name"]) as? String)!
                index.email = ((dictionary["email"]) as? String)!
                index.phone = ((dictionary["phone"]) as? String)!
                index.password = ((dictionary["password"]) as? String)!
                self.userinfoArray.append(index)
                
                let url = URLRequest(url: URL(string: index.profileImageUrl)!)
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    DispatchQueue.main.async {
                        self.userBackgroundImage.image = UIImage(data: data!)
                        self.userProfilePic.image = UIImage(data: data!)
                        self.nameTextField.text = index.name
                        self.emailTextField.text = index.email
                        self.phoneTextField.text = index.phone
                        self.passwordTextField.text = index.password
                    }
                }
                task.resume()
            }
            print(snapshot)
            
        }, withCancel: nil)
    }
    
    @IBAction func saveSettings(_ sender: UIButton) {
        
        SVProgressHUD.show()
        saveEditedSettings()
        SVProgressHUD.dismiss()
        
    
    }
    
    func saveEditedSettings() {
        
        let uid = Auth.auth().currentUser?.uid
        let values = ["name": nameTextField.text!, "email": emailTextField.text!, "phone": phoneTextField.text!, "password": passwordTextField.text! ]
        
        let ref = Database.database().reference().child("USERS").child(uid!)
        ref.updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("Updated information")
                self.performSegue(withIdentifier: "DogMainCode", sender: self)
            } else {
              print("Could not update.")
            }
            
        })

    }


        override var preferredStatusBarStyle : UIStatusBarStyle {
            return .lightContent
        }
    
    @IBAction func goBackAction( _sender: UIButton) {
        self.performSegue(withIdentifier: "DogMainCode", sender: self)
    }
        
}



