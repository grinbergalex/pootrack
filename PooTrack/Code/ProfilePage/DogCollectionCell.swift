//
//  DogCollectionCell.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class DogCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var dogNameText: UILabel!
    @IBOutlet weak var dogKmsText: UILabel!
    @IBOutlet weak var dogPooText: UILabel!
    @IBOutlet weak var dogPeeText: UILabel!
    @IBOutlet weak var generalImage: UIImage!
    
}
