//
//  ImageCollectionCell.swift
//  PooTrack
//
//  Created by Alex Grinberg on 14/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var dogsImages: UIImageView!
    
}
