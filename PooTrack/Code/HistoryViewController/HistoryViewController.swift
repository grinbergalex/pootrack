//
//  HistoryViewController.swift
//  PooTrack
//
//  Created by Alex Grinberg on 10/03/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import SVProgressHUD

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var historyArray = [HistoryModel]()
    
    let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .singleLine
        table.backgroundColor = .clear
        table.clipsToBounds = true
        table.layer.cornerRadius = 40
        table.cornerRadius = 40
        
        return table
        
    }()
    
    let topBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.rgb(red: 69, green: 155, blue: 218)
        
        return view
        
    }()
    
    let backgroundLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.text = "Select which dog you are walking"
        label.font = UIFont(name: "Avenir-Next", size: 14)
        
        return label
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.rgb(red: 250, green: 251, blue: 252)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(HistoryTableViewCell.self, forCellReuseIdentifier: "HistoryTableViewCell")
    }
    
    func fetchUserHistory() {
        
        let uid = Auth.auth().currentUser?.uid
        
        let DB_BASE = Database.database().reference().child("USERS").child(uid!).child("Walks")
        DB_BASE.observe(.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let history = HistoryModel()
                history.imageUrl = (dictionary["profileImageUrl"] as! String?)!
                history.dogUid = (dictionary["doguid"] as! String?)!
                history.name = (dictionary["dogname"] as! String?)!
                self.historyArray.append(history)
                
                if snapshot.value != nil {
                    let url = URLRequest(url: URL(string: history.imageUrl)!)
                    
                    let task = URLSession.shared.dataTask(with: url) {
                        (data, response, error) in
                        if error != nil {
                            print(error!)
                            return
                        }
                    }
                    task.resume()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        self.tableView.reloadData()
                    }
                } else {
                    print("No Data")
                }
            }
            print(snapshot)
            
        }, withCancel: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        cell.backgroundColor = .clear
        
        let index = historyArray[indexPath.row].imageUrl
        let dogname = historyArray[indexPath.row].name
        cell.distanceLabel.text = dogname
        
        let url = URLRequest(url: URL(string: index)!)
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                cell.profileImageView.image = UIImage(data: data!)
            }
            
        }
        
        task.resume()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    

}

