//
//  HistoryTableViewCell.swift
//  PooTrack
//
//  Created by Alex Grinberg on 10/03/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class HistoryTableViewCell: UITableViewCell {
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 40
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.masksToBounds = true
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let dogNameLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Dog Name"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let weekDayLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Upload your picture"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let durationLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Upload your picture"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    let distanceLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.contentMode = .scaleAspectFit
        label.textAlignment = .center
        label.text = "Upload your picture"
        label.font = UIFont(name: "GillSans", size: 17)
        return label
        
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(containerView)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(profileImageView)
        profileImageView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 10, paddingRight: 0, width: 0, height: 80)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
